import React, { useEffect, useState } from "react";
import { connect, useDispatch, useSelector } from "react-redux";
import { getPaymentByID } from "./../actions/index";
// import { useHistory } from "react-router-dom";

function FindPaymentPage(props) {
	// const history = useHistory();
	const [paymentId, setPaymentId] = useState("");

	const dispatch = useDispatch();

	const statusInfo = useSelector((state) => state.status.result);
	useEffect(() => {
		console.log(statusInfo);
	}, [statusInfo]);

	const handleSubmit = (e) => {
		e.preventDefault();
		if (paymentId) {
			dispatch(getPaymentByID(paymentId));
		}
	};

	const isEmpty = (obj) => {
		return Object.keys(obj).length === 0;
	};

	return (
		<div className="jumbotron">
			{!isEmpty(statusInfo) && statusInfo.message && (
				<div className="alert alert-primary" role="alert">
					{statusInfo.message}
				</div>
			)}
			<div className="container" style={{ marginTop: 10, marginBottom: 50 }}>
				<h4 className="page-header">Find Payment By Id</h4>
				<form onSubmit={handleSubmit} autoComplete="off">
					<div className="table-responsive">
						<table className="table">
							<tbody>
								<tr>
									<td>
										<label htmlFor="title">Payment Id:</label>
									</td>
									<td>
										<input
											id="paymentId"
											type="text"
											className="form-control"
											value={paymentId}
											onChange={(e) => setPaymentId(e.target.value)}
										/>
									</td>
									<td>
										<input
											type="submit"
											value="Search"
											className="btn btn-primary"
										/>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</form>
			</div>
			{!isEmpty(statusInfo) && !statusInfo.message && (
				<div className="table-responsive">
					<table className="table">
						<thead>
							<tr>
								<th scope="col">Customer ID</th>
								<th scope="col">Payment Id</th>
								<th scope="col">Payment Date</th>
								<th scope="col">Payment Type</th>
								<th scope="col">Amount</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th scope="row">{statusInfo.custId}</th>
								<td>{statusInfo.id}</td>
								<td>{statusInfo.paymentDate}</td>
								<td>{statusInfo.type}</td>
								<td>{statusInfo.amount}</td>
							</tr>
						</tbody>
					</table>
				</div>
			)}
		</div>
	);
}

const mapStateToProps = (state) => {
	return {
		result: state.status.result,
	};
};

const actionCreators = {
	getPaymentByID,
};

export default connect(mapStateToProps, actionCreators)(FindPaymentPage);
