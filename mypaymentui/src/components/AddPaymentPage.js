import React, { useEffect, useState } from "react";
import { connect, useDispatch, useSelector } from "react-redux";
import { savePayment } from "../actions/index";
// import { useHistory } from "react-router-dom";

function AddPaymentPage(props) {
	// const history = useHistory();
	const [paymentId, setPaymentId] = useState("");
	const [customerId, setCustomerId] = useState("");
	const [paymentType, setPaymentType] = useState("");
	const [amount, setAmount] = useState("");

	const dispatch = useDispatch();

	const statusInfo = useSelector((state) => state.status.result);
	useEffect(() => {
		console.log(statusInfo);
	}, [statusInfo]);

	const handleSubmit = (e) => {
		e.preventDefault();
		if (paymentId && customerId && paymentType && amount) {
			let paymentInfo = {
				id: paymentId,
				custId: customerId,
				type: paymentType,
				amount: amount,
				paymentDate: new Date(),
			};
			dispatch(savePayment(paymentInfo));
		}
	};

	const isEmpty = (obj) => {
		return Object.keys(obj).length === 0;
	};

	return (
		<div className="jumbotron">
			{!isEmpty(statusInfo) && statusInfo.message && (
				<div className="alert alert-primary" role="alert">
					{statusInfo.message}
				</div>
			)}
			<div className="container" style={{ marginTop: 10, marginBottom: 50 }}>
				<h4 className="page-header"> Enter Payment Details</h4>
				<form onSubmit={handleSubmit} autoComplete="off">
					<div className="form-row">
						<div className="form-group col-md-5">
							<label htmlFor="title">Payment Id:</label>
							<input
								id="paymentId"
								type="text"
								className="form-control"
								value={paymentId}
								onChange={(e) => setPaymentId(e.target.value)}
							/>
						</div>
						<div className="form-group col-md-5">
							<label htmlFor="custmerId">Customer Id:</label>
							<input
								id="custmerId"
								type="text"
								className="form-control"
								value={customerId}
								onChange={(e) => setCustomerId(e.target.value)}
							/>
						</div>
					</div>
					<div className="form-row">
						<div className="form-group col-md-5">
							<label htmlFor="paymentType">Payment Type:</label>
							<input
								id="paymentType"
								type="text"
								className="form-control"
								value={paymentType}
								onChange={(e) => setPaymentType(e.target.value)}
							/>
						</div>
						<div className="form-group col-md-5">
							<label htmlFor="amount">Amount:</label>
							<input
								id="amount"
								type="text"
								className="form-control"
								value={amount}
								onChange={(e) => setAmount(e.target.value)}
							/>
						</div>
					</div>
					<div className="form-row">
						<div className="form-group col-md-5">
							<input type="submit" value="Save" className="btn btn-primary" />
						</div>
					</div>
				</form>
			</div>
		</div>
	);
}

const mapStateToProps = (state) => {
	return {
		result: state.status.result,
	};
};

const actionCreators = {
	savePayment,
};

export default connect(mapStateToProps, actionCreators)(AddPaymentPage);
