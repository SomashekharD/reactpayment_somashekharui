import "./App.css";
import Header from "./common/Header";
import HomePage from "./HomePage";
import FindPaymentPage from "./FindPaymetPage";
import { Switch, Route } from "react-router-dom";
import AddPaymentPage from "./AddPaymentPage";

function App() {
	return (
		<div className="app">
			<div className="container">
				<div className="row">
					<Header />
				</div>
				<Switch>
					<Route path="/" exact component={HomePage} />
					<Route path="/search" component={FindPaymentPage} />
					<Route path="/add" component={AddPaymentPage} />
				</Switch>
			</div>
		</div>
	);
}

export default App;
