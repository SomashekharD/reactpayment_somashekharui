import React from "react";
import { NavLink } from "react-router-dom";

function Header() {
	const activeStyle = { color: "orange" };
	return (
		<nav>
			<NavLink activeStyle={activeStyle} exact to="/">
				About Us
			</NavLink>{" "}
			|{" "}
			<NavLink activeStyle={activeStyle} exact to="/add">
				Add Payment
			</NavLink>{" "}
			|{" "}
			<NavLink activeStyle={activeStyle} exact to="/search">
				Find Payments
			</NavLink>{" "}
		</nav>
	);
}

export default Header;
