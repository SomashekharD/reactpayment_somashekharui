import React, { useEffect } from "react";
import { connect, useDispatch, useSelector } from "react-redux";
import { getPaymentStatus } from "../actions/index";
import { Link } from "react-router-dom";

function HomePage(props) {
	const dispatch = useDispatch();

	const statusInfo = useSelector((state) => state.status.status);
	useEffect(() => {
		dispatch(getPaymentStatus());
	});

	return (
		<div className="jumbotron">
			<div className="row">
				<h3>Payment Status</h3>
			</div>
			<div className="row"></div>
			<div className="row">
				{!statusInfo && <h4>Payment API are Down</h4>}
				{statusInfo && (
					<div>
						<h4>Payment API are up and Running</h4>
						<br />
						<div className="row"> </div>
						<p>
							Add a Payment{" "}
							<Link to="/add" className="btn btn-primary">
								Add
							</Link>
						</p>
						<p>
							Find a Payment{" "}
							<Link to="/search" className="btn btn-primary">
								Find
							</Link>
						</p>
					</div>
				)}
			</div>
		</div>
	);
}

const mapStateToProps = (state) => {
	return {
		status: state.status.status,
	};
};

const actionCreators = {
	getPaymentStatus,
};

export default connect(mapStateToProps, actionCreators)(HomePage);
