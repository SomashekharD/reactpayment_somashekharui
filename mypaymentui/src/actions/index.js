import axios from "axios";

export const getPaymentStatusSuccess = (status) => {
	return {
		type: "GET_PAYMENT_STATUS_SUCCESS",
		payload: status,
	};
};

export const getPaymentStatusFailure = (err) => {
	return {
		type: "GET_PAYMENT_STATUS_FAILURE",
		payload: { message: "Failed to fetch payments.. please try again later" },
	};
};

export const getPaymentByIdSuccess = (result) => {
	return {
		type: "GET_PAYMENT_BYID_SUCCESS",
		payload: result,
	};
};

export const getPaymentByIdFailure = (err) => {
	return {
		type: "GET_PAYMENT_BYID_FAILURE",
		payload: { message: "Failed to fetch payments.. please try again later" },
	};
};

export const savePaymentSuccess = (success) => {
	return {
		type: "SAVE_PAYMENT_SUCCESS",
		payload: { message: "Successfully Saved" },
	};
};

export const savePaymentFailure = (err) => {
	return {
		type: "SAVE_PAYMENT_FAILURE",
		payload: { message: "Failed to save payments.. please try again later" },
	};
};

// to be call by the components
export const getPaymentStatus = () => {
	// returns the thunk function
	return (dispatch, getState) => {
		axios.get("http://localhost:8080/api/payment/status").then(
			(res) => {
				setTimeout(() => {
					dispatch(getPaymentStatusSuccess(res.data));
					console.log("state after getPaymentStatusSuccess", getState());
				});
			},
			(err) => {
				dispatch(getPaymentStatusFailure(false));
				console.log("state after getPaymentStatusFailure", getState());
			}
		);
	};
};

export const getPaymentByID = (id) => {
	// returns the thunk function
	return (dispatch, getState) => {
		axios.get("http://localhost:8080/api/payment/findbyid/" + id).then(
			(res) => {
				setTimeout(() => {
					dispatch(getPaymentByIdSuccess(res.data));
					console.log("state after getPaymentByIdSuccess", getState());
				});
			},
			(err) => {
				dispatch(getPaymentByIdFailure(err));
				console.log("state after getPaymentByIdFailure", getState());
			}
		);
	};
};

export const savePayment = (paymentInfo) => {
	// returns the thunk function
	return (dispatch, getState) => {
		axios.post("http://localhost:8080/api/payment/save", paymentInfo).then(
			(res) => {
				setTimeout(() => {
					dispatch(savePaymentSuccess(res.data));
					console.log("state after savePaymentSuccess", getState());
				});
			},
			(err) => {
				dispatch(savePaymentFailure(err));
				console.log("state after savePaymentFailure", getState());
			}
		);
	};
};
