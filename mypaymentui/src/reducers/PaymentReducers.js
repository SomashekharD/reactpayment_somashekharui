const initialState = { status: "", result: {} };

export default (state = initialState, action) => {
	console.log(`received ${action.type} dispatch in albumsReducer`);
	switch (action.type) {
		case "GET_PAYMENT_STATUS_SUCCESS":
			return { ...state, status: action.payload, loading: false };
		case "GET_PAYMENT_STATUS_FAILURE":
			return { ...state, status: "", loading: false, error: action.payload };
		case "GET_PAYMENT_BYID_SUCCESS":
			return { ...state, result: action.payload, loading: false };
		case "GET_PAYMENT_BYID_FAILURE":
			return {
				...state,
				result: action.payload,
				loading: false,
				error: action.payload,
			};
		case "SAVE_PAYMENT_SUCCESS":
			return { ...state, result: action.payload, loading: false };
		case "SAVE_PAYMENT_FAILURE":
			return {
				...state,
				result: action.payload,
				loading: false,
				error: action.payload,
			};

		default:
			return state;
	}
};
