import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import "bootstrap/dist/css/bootstrap.css";
import { BrowserRouter as Router } from "react-router-dom";
import { createStore, combineReducers, applyMiddleware } from "redux";
import paymentReducer from "./reducers/PaymentReducers";
import { Provider } from "react-redux";
import thunkMiddleware from "redux-thunk";
const reducers = combineReducers({
	status: paymentReducer,
});

const store = createStore(reducers, applyMiddleware(thunkMiddleware));
console.log("store is ", store.getState());

ReactDOM.render(
	<Router>
		<Provider store={store}>
			<App />
		</Provider>
	</Router>,
	document.getElementById("root")
);
